public class TestSquareArea {
    public static void main() {
        Area2D square = new Square(8d);
        assert calculateArea(square) == 64d;
    }


    private static double calculateArea(Area2D area) {
        return area.getArea();
    }
}