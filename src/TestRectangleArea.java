public class TestRectangleArea {
    public static void main() {
        Area2D rectangle = new Rectangle(2d, 5d);
        assert calculateArea(rectangle) == 10d;
    }

    private static double calculateArea(Area2D area) {
        return area.getArea();
    }
}